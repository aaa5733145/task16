﻿#include <iostream>

class Animal 
{
private:
    std::string x;
public:
    Animal()
    {}
    Animal(std::string _x):x(_x)
    {}

    virtual void Voice()
    {
        std::cout << x << "\n";
    }


};

class Dog:public Animal
{
private:
    std::string animalsound = "Woff!";
public:
    Dog()
    {}
    Dog(std::string _animalsound):animalsound(_animalsound)
    {}
    void Voice() override
    {
        std::cout << animalsound << "\n";
    }
};

class Cat :public Animal
{
private:
    std::string animalsound = "Miau!";
public:
    Cat()
    {}
    Cat(std::string _animalsound) :animalsound(_animalsound)
    {}
    void Voice() override
    {
        std::cout << animalsound << "\n";
    }
};

class Cow :public Animal
{
private:
    std::string animalsound = "Muuu!";
public:
    Cow()
    {}
    Cow(std::string _animalsound) :animalsound(_animalsound)
    {}
    void Voice() override
    {
        std::cout << animalsound << "\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Cow;
    for (int i = 0; i < 3; i++)
    {
        animals[i]->Voice();
    }
}